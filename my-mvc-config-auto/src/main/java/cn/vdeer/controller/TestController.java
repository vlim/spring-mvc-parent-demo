package cn.vdeer.controller;

import cn.vdeer.entity.Student;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
    @Resource
    private Student student;

    /**
     * 测试 Spring 配置是否生效
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/test")
    public Student get() {
        return student;
    }

    /**
     * 测试异常映射是否生效
     *
     * @return
     */
    @GetMapping("/testException")
    public Student get2() {
        int a = 10 / 0;
        return student;
    }
}
