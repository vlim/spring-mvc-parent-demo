package cn.vdeer.entity;

import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

public class Student {
    //学号
    private String stuId;
    //姓名
    private String stuName;
    //出生日期
    private Date birth;
    //身高
    private Double height;
    //用于接收后台上传的文件
    private List<MultipartFile> photos;
    //文件名称的字符串
    private String fileNameStr;
    //已上传图片的路径集合
    private List<String> paths;

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public List<MultipartFile> getPhotos() {
        return photos;
    }

    public void setPhotos(List<MultipartFile> photos) {
        this.photos = photos;
    }

    public String getFileNameStr() {
        return fileNameStr;
    }

    public void setFileNameStr(String fileNameStr) {
        this.fileNameStr = fileNameStr;
    }

    public List<String> getPaths() {
        return paths;
    }

    public void setPaths(List<String> paths) {
        this.paths = paths;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stuId='" + stuId + '\'' +
                ", stuName='" + stuName + '\'' +
                ", birth=" + birth +
                ", height=" + height +
                ", photos=" + photos +
                ", fileNameStr='" + fileNameStr + '\'' +
                ", paths=" + paths +
                '}';
    }
}
