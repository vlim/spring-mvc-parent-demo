package cn.vdeer.config;

import cn.vdeer.entity.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class SpringConfig {
    @Bean
    public Student getStudent() throws ParseException {
        Student student = new Student();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date birth = format.parse("1990-06-15");
        student.setStuId("1001");
        student.setStuName("小明");
        student.setBirth(birth);
        student.setHeight(1.80);
        return student;
    }
}
