package cn.vdeer.config;

import jakarta.servlet.Filter;
import jakarta.servlet.MultipartConfigElement;
import jakarta.servlet.ServletRegistration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebInit extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * 设置 Spring 的配置类
     *
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{SpringConfig.class};
    }

    /**
     * 设置 Spring MVC 的配置类
     *
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{SpringWebMvcConfig.class};
    }

    /**
     * 为 DispatcherServlet 指定映射规则，相当于 web.xml 中配置的 url-pattern
     *
     * @return
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    /**
     * 添加过滤器
     *
     * @return
     */
    @Override
    protected Filter[] getServletFilters() {
        HiddenHttpMethodFilter hiddenHttpMethodFilter = new HiddenHttpMethodFilter();
        return new Filter[]{hiddenHttpMethodFilter};
    }

    /**
     * 配置文件上传的临时目录和文件大小限制
     *
     * @param registration
     */
    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        //org.springframework.web.multipart.MultipartException: Failed to parse multipart servlet request;
        //该异常为何会产生
        //我的应用中没有文件上传，为啥会抛org.springframework.web.multipart.MultipartException异常
        //1）首先，我们应该知道，对于http POST请求来说，它需要使用这个临时目录来存储post数据。
        //2）其次，因为该目录是挂在到/temp目录下的临时文件，那么对于一些OS系统，像centOS将经常删除这个临时目录，所有导致该目录不存在了
        //
        MultipartConfigElement multipartConfig = new MultipartConfigElement("d:/tmp/", 2097152, 4194304, 0);
        registration.setMultipartConfig(multipartConfig);
        registration.setLoadOnStartup(1);
    }
}
