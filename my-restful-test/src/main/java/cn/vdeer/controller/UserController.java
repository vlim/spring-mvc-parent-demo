package cn.vdeer.controller;

import cn.vdeer.entity.User;
import cn.vdeer.service.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("userController")
public class UserController {
    @Resource
    private UserService userService;

    @RequestMapping("/login")
    private String login(User user, HttpServletRequest request) {
        User LoginedUser = userService.selectUserByUserName(user);
        if (LoginedUser != null && LoginedUser.getPassword().equals(user.getPassword())) {
            HttpSession session = request.getSession();
            //将用户信息放到 session 域中
            session.setAttribute("loginUser", LoginedUser);
            //重定向到商品列表
            return "redirect:/products";
        }
        request.setAttribute("msg", "账号或密码错误");
        return "login";
    }
}
