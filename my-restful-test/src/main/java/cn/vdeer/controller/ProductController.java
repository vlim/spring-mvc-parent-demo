package cn.vdeer.controller;

import cn.vdeer.entity.Product;
import cn.vdeer.service.ProductService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller("productController")
public class ProductController {
    @Resource
    private ProductService productService;

    @RequestMapping("/products")
    public String setProductService(HttpServletRequest request) {
        List<Product> productList = productService.getProductList();
        request.setAttribute("productList", productList);
        return "product_list";
    }

    @RequestMapping("/product/{todo}/{pid}")
    public String getProduct(@PathVariable("todo") String todo, @PathVariable("pid") String pid, HttpServletRequest request) {
        Product product = productService.getProductById(pid);
        request.setAttribute("product", product);
        switch (todo) {
            case "info" -> {
                return "product_info";
            }
            case "update" -> {
                return "product_update";
            }
        }
        return "product_list";
    }

    @PutMapping("/product")
    public String updateProduct(Product product, HttpServletRequest request) {
        productService.updateProduct(product);
        return "redirect:/products";
    }

    @DeleteMapping("/product")
    public String deleteProduct(HttpServletRequest request) {
//        System.out.println("删除PID：" + request.getParameter("prodId"));
        productService.deleteProduct(request.getParameter("prodId"));
        return "redirect:/products";
    }

    @PostMapping("/product")
    public String addProduct(Product product) {
        productService.addProduct(product);
        return "redirect:/products";
    }

}
