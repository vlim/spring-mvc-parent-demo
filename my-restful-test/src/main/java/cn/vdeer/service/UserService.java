package cn.vdeer.service;

import cn.vdeer.entity.User;

public interface UserService {
    /**
     * 查询用户
     *
     * @param user
     * @return
     */
    User selectUserByUserName(User user);
}
