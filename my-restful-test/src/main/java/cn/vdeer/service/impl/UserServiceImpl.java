package cn.vdeer.service.impl;

import cn.vdeer.dao.UserDao;
import cn.vdeer.entity.User;
import cn.vdeer.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    /**
     * 查询用户
     *
     * @param user
     * @return
     */
    @Override
    public User selectUserByUserName(User user) {
        return userDao.getUserByUserName(user);
    }
}
