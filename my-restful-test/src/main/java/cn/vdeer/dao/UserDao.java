package cn.vdeer.dao;

import cn.vdeer.entity.User;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository("userDao")
public class UserDao {
    private static Map<String, User> users = null;

    static {
        users = new HashMap<String, User>();
        User user = new User();
        user.setUserId("1001");
        user.setUserName("Lim");
        user.setPassword("123567");
        User user2 = new User();
        user2.setUserId("1002");
        user2.setUserName("admin");
        user2.setPassword("admin");
        users.put(user.getUserName(), user);
        users.put(user2.getUserName(), user2);
    }

    /**
     * 根据用户名获取用户信息
     *
     * @param user
     * @return
     */
    public User getUserByUserName(User user) {
        User loginUser = users.get(user.getUserName());
        return loginUser;
    }

}
