package cn.vdeer.stu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/myReqMap")
public class MyReqMapController {
    @RequestMapping(value = {"/welcome", "/do"}, name = "跳转到欢迎页", method = RequestMethod.GET)
    public String testValue() {
        return "welcome";
    }

    @RequestMapping(value = "/testParam", params = {"name=helloWorld", "url=https://www.vdeer.cn/"})
    public String testParam() {
        return "param";
    }

    @RequestMapping(value = "/testHeader", headers = {"Content-Type=application/json", "Referer=https://www.vdeer.cn/"})
    public String testHeader() {
        return "header";
    }
}
