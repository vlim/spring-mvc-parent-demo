package cn.vdeer.stu.controller;

import cn.vdeer.stu.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/myEntityParam")
public class MyEntityParamController {
    @RequestMapping("/")
    public String getEntity() {
        return "toParam";
    }

    @RequestMapping("/getUser")
    public String getEntityParam(User user) {
        System.out.println("userId：" + user.getUserId());
        System.out.println("userName：" + user.getUserName());
        System.out.println("password：" + user.getPassword());
        return "toParam";
    }

}
