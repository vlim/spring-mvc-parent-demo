package cn.vdeer.stu.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Iterator;

@Controller
@RequestMapping("/myHttpReq")
public class MyHttpReqController {
    @RequestMapping("/testHttpParam")
    public String testHttpParam(HttpServletRequest request) {

        for (Iterator<String> it = request.getParameterNames().asIterator(); it.hasNext(); ) {
            String pname = it.next();
            System.out.println(pname + "::" + request.getParameter(pname));

        }
//        String name = request.getParameter("name");
//        String url = request.getParameter("url");
//        System.out.println("name:" + name);
//        System.out.println("url:" + url);

        return "hparam";
    }
}
