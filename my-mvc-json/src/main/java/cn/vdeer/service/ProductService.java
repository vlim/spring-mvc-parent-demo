package cn.vdeer.service;

import cn.vdeer.entity.Product;

import java.util.List;

public interface ProductService {
    /**
     * 查询商品列表
     *
     * @return
     */
    List<Product> getProductList();

    //根据商品 ID 查询商品
    Product getProductById(String productId);

    /**
     * 新增商品
     *
     * @param product
     */
    void addProduct(Product product);

    /**
     * 修改商品信息
     *
     * @param product
     */
    void updateProduct(Product product);

    /**
     * 删除商品
     *
     * @param productId
     */
    void deleteProduct(String productId);
}
