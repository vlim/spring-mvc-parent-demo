package cn.vdeer.controller;

import cn.vdeer.entity.Product;
import cn.vdeer.service.ProductService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ProductController {
    @Resource
    private ProductService productService;

    /**
     * 查看或回显商品信息，get：查看商品信息  update:为修改页回显的商品信息
     */
    @ResponseBody
    @RequestMapping("/product/{productId}")
    public Product getProduct(@PathVariable("productId") String productId) {
        Product product = productService.getProductById(productId);
        return product;
    }

    /**
     * 新增商品
     *
     * @param product
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public Product addProduct(@RequestBody Product product) {
        productService.addProduct(product);
        return product;
    }

    /**
     * 删除指定的商品
     *
     * @param prodId
     * @return
     */
    @RequestMapping(value = "/product", method = RequestMethod.DELETE)
    public String deleteProduct(String prodId) {
//        System.out.println("productId:" + productId);
        System.out.println("prodId:" + prodId);
        productService.deleteProduct(prodId);
        return "redirect:/products";
    }

    /**
     * 获取商品列表
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/getProductList1")
    public List<Product> getProductList1() {
        List productList = productService.getProductList();
        return productList;
    }

    /**
     * 修改商品信息
     *
     * @param product
     * @return
     */
    @RequestMapping(value = "/edit-product")
    public String updateProduct(Product product) {
        productService.updateProduct(product);
        return "redirect:/products";
    }
}
