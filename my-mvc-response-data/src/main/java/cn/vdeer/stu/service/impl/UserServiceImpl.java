package cn.vdeer.stu.service.impl;

import cn.vdeer.stu.dao.UserDao;
import cn.vdeer.stu.entity.User;
import cn.vdeer.stu.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    @Override
    public User getUserByUserName(String userName) {
        return userDao.getUserByUserName(userName);
    }
}
