package cn.vdeer.stu.service;

import cn.vdeer.stu.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProductList();

    Product getProductById(Integer productId);
}
