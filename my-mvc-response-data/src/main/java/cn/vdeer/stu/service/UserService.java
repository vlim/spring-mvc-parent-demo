package cn.vdeer.stu.service;

import cn.vdeer.stu.entity.User;

public interface UserService {
    User getUserByUserName(String userName);
}
