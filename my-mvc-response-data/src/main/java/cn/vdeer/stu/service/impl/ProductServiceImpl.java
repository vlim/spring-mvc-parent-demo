package cn.vdeer.stu.service.impl;

import cn.vdeer.stu.dao.ProductDao;
import cn.vdeer.stu.entity.Product;
import cn.vdeer.stu.service.ProductService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("productService")
public class ProductServiceImpl implements ProductService {

    @Resource
    private ProductDao productDao;

    @Override
    public List<Product> getProductList() {
        return productDao.getProductList();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productDao.getProductById(productId);
    }
}
