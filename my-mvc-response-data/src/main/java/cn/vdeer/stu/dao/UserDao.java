package cn.vdeer.stu.dao;

import cn.vdeer.stu.entity.User;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserDao {
    private static Map<String, User> users = null;

    static {
        users = new HashMap<String, User>();
        User user1 = new User();
        user1.setUserId("1001");
        user1.setUserName("Limcreate");
        user1.setPassword("123567");

        User user2 = new User();
        user2.setUserId("1003");
        user2.setUserName("Java用户");
        user2.setPassword("987654321");
        User user3 = new User();
        user3.setUserId("1004");
        user3.setUserName("李小龙");
        user3.setPassword("1qazxsw2");

        users.put(user1.getUserName(), user1);
        users.put(user2.getUserName(), user2);
        users.put(user3.getUserName(), user3);
    }

    public List getUserList() {
        List userList = new ArrayList();
        Set<String> keys = users.keySet();
        for (String key : keys) {
            User user = users.get(key);
            userList.add(user);
        }
        return userList;
    }

    public User getUserByUserName(String userName) {
        User user = users.get(userName);
        return user;
    }

    public User getUserByUserId(String userId) {
        List userList = new ArrayList();
        Set<String> keys = users.keySet();
        for (String key : keys) {
            User user = users.get(key);
            if (user.getUserId().equals(userId)) {
                return user;
            }
        }
        return null;
    }
    
    public void addUser(User user) {
        users.put(user.getUserName(), user);
    }

    public void update(User user) {
        users.put(user.getUserName(), user);
    }

    public void delete(String userId) {
        User user = getUserByUserId(userId);
        users.remove(user.getUserName());
    }
}
