package cn.vdeer.stu.controller;

import cn.vdeer.stu.entity.Product;
import cn.vdeer.stu.service.ProductService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ProductController {
    @Resource
    private ProductService productService;

    @RequestMapping("/getProductList")
    public ModelAndView getProductList() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("productList");
        List<Product> productList = productService.getProductList();
        //  在控制器方法中创建一个 ModelAndView 对象，并调用它的 addObject() 等方法对数据的进行封装，
        //  这样 Spring MVC 就会自动将 ModelAndView 中的数据存储到 request 域对象中
        modelAndView.addObject(productList);
        return modelAndView;
    }

    @RequestMapping("/getProduct")
    public String getProduct(Integer productId, Model model) {
        Product productById = productService.getProductById(productId);
        model.addAttribute("product", productById);
        return "product";
    }
}
