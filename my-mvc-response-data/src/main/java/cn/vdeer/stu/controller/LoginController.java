package cn.vdeer.stu.controller;

import cn.vdeer.stu.entity.User;
import cn.vdeer.stu.service.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
    @Resource
    private UserService userService;

    @RequestMapping(value = {"/user", "/"})
    public String sayHello() {
        return "user";
    }

    @RequestMapping("/login")
    public String login(User user, HttpServletRequest request) {
        User user2 = userService.getUserByUserName(user.getUserName());
        if (user2 != null && user2.getPassword().equals(user.getPassword())) {
            HttpSession session = request.getSession();
            session.setAttribute("loginUser", user2);
            return "redirect:/getProductList";
        }
        request.setAttribute("msg", "账号或密码错误！");
        return "user";
    }

}
