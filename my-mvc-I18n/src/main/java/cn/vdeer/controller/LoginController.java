package cn.vdeer.controller;

import cn.vdeer.entity.User;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String toLogin(User user, HttpServletRequest request) {
        if ("admin".equals(user.getUserName()) && "admin".equals(user.getPassword())) {
            HttpSession session = request.getSession();
            session.setAttribute("loginUser", user);
            return "redirect:success";
        }
        request.setAttribute("msg", "用户名或密码错误！");
        return "login";
    }
}
