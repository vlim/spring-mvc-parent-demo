package cn.vdeer.controller;

import jakarta.annotation.Resource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Locale;

@Controller
public class I18nController {
    @Resource
    private ResourceBundleMessageSource messageSource;

    //切换语言环境
    @RequestMapping("/localeChange")
    public String localeChange(Locale locale) {
        String userName = messageSource.getMessage("userName", null, locale);
        String password = messageSource.getMessage("password", null, locale);
        String submit = messageSource.getMessage("submit", null, locale);
        String reset = messageSource.getMessage("reset", null, locale);
        String error = messageSource.getMessage("error", null, locale);
        System.out.println(userName + "----" + password + "----" + submit + "----" + reset + "----" + error); //debug
        return "login";
    }
}
