package cn.vdeer.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

@Controller
public class DownLoadController {
    /**
     * 文件下载
     *
     * @param request
     * @param fileName
     * @return
     * @throws IOException
     */
    @RequestMapping("/downLoadFile")
    public ResponseEntity<byte[]> getdownLoadFile(HttpServletRequest request, String fileName) {
        //得到图片的实际路径
        String realPath = request.getServletContext().getRealPath("/upload/" + fileName);
        //创建该图片的对象
        File file = new File(realPath);
        //将图片数据读取到字节数组中
        byte[] bytes;
        try {
            bytes = FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        //创建 HttpHeaders 对象设置响应头信息
        HttpHeaders httpHeaders = new HttpHeaders();
        //设置图片下载的方式和文件名称
        httpHeaders.setContentDispositionFormData("attachment", toUTF8String(fileName));
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
    }

    /**
     * 下载保存时中文文件名的字符编码转换方法
     */
    private String toUTF8String(String fileName) {
        StringBuffer sb = new StringBuffer();
        int len = fileName.length();
        for (int i = 0; i < len; i++) {
            // 取出字符中的每个字符
            char c = fileName.charAt(i);
            // Unicode码值为0~255时，不做处理
            if (c >= 0 && c <= 255) {
                sb.append(c);
            } else {
                // Unicode码值不在0~255范围，则转换为 UTF-8 编码
                byte b[];
                try {
                    b = Character.toString(c).getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    b = null;
                }
                // 转换为%HH的字符串形式
                for (int j = 0; j < b.length; j++) {
                    int k = b[j];
                    if (k < 0) {
                        k &= 255;
                    }
                    sb.append("%" + Integer.toHexString(k).toUpperCase());
                }
            }

        }
        return sb.toString();
    }


}
