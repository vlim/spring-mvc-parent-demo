package cn.vdeer.controller;

import cn.vdeer.entity.Student;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

@MultipartConfig(location = "/tmp/")
@Controller
public class MultiFileController {

    /**
     * 图片上传
     *
     * @param photo
     * @param request
     * @return
     */
    @RequestMapping(value = "/uploadPhoto", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, String> uploadPhoto(MultipartFile photo, HttpServletRequest request) {
        Map<String, String> ret = new HashMap<String, String>();
        //如果上传文件为空，提示错误信息
        if (photo == null) {
            ret.put("type", "error");
            ret.put("msg", "选择要上传的文件！");
            return ret;
        }
        //检测上传文件大小 不大于10M
        if (photo.getSize() > 1024 * 1024 * 10) {
            ret.put("type", "error");
            ret.put("msg", "文件大小不能超过10M！");
            return ret;
        }
        //获取文件后缀
        String suffix = photo.getOriginalFilename().substring(photo.getOriginalFilename().lastIndexOf(".") + 1, photo.getOriginalFilename().length());
        if (!"jpg,jpep,gif,png".toUpperCase().contains(suffix.toUpperCase())) {
            ret.put("type", "error");
            ret.put("msg", "请选择jpg、peg、gif、png 格式的图片！");
            return ret;
        }
        //设置上传文件真实路径
        String realPath = request.getServletContext().getRealPath("/upload/");
        System.out.println(realPath);  //后台监测
        //创建保存上传文件的目录
        File fileDir = new File(realPath);
        if (!fileDir.exists()) {
            fileDir.mkdir();
        }
        //设置上传文件名称
        String filename = photo.getOriginalFilename();
        System.err.println("正在上传的图片为：" + filename); //后台监测
        String newFileName = UUID.randomUUID() + filename;
        //保存上传文件操作
        try {
            //将文件保存指定目录
            photo.transferTo(new File(realPath + newFileName));

        } catch (Exception e) {
            ret.put("type", "error");
            ret.put("msg", "保存文件异常！");
            e.printStackTrace();
            return ret;
        }
        ret.put("type", "success");
        ret.put("msg", "上传图片成功！");
        ret.put("filepath", "upload/");
        ret.put("filename", newFileName);
        return ret;
    }

    /**
     * 提交学生信息
     *
     * @param student
     * @param model
     * @return
     */
    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public String uploadFile(Student student, Model model, HttpServletRequest request) {
        String fileNameStr = student.getFileNameStr();
        //将图片路径的字符串拆分，添加到图片路径的集合中
        String[] split = fileNameStr.split(",");
        List<String> list = new ArrayList<>();
        for (String fileName : split) {
            list.add(fileName);
        }
        System.out.println(list.toString());//后台监测
        student.setPaths(list);
        model.addAttribute("student", student);
        model.addAttribute("contextPath", request.getContextPath() + "/upload/");
        return "success1";
    }


}
