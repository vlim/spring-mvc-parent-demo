package cn.vdeer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "用户不存在")
public class UserNotExistException extends RuntimeException {
    @Override
    public String toString() {
        return this.getClass() + ":" + "指定用户名，用户不存在！";
    }
}
