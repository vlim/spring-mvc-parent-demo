package cn.vdeer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 注意，定义在某个控制器类中的 @ExceptionHandler 方法只在当前的控制器中有效，
 * 它只能处理其所在控制器类中发生的异常。
 */
@Controller
public class ExceptionController {
    //控制器方法
    @RequestMapping(value = "/testExceptionHandler1")
    public String testExceptionHandler1() {
        //发生 ArithmeticException 异常
        System.out.println(10 / 0);
        return "success";
    }

    //使用 @ExceptionHandler 注解定义一个异常处理方法
//    @ExceptionHandler(value = {Exception.class, ArithmeticException.class})
    @ExceptionHandler(value = {Exception.class})
    public String handleException(Exception exception, Model model) {
        //将异常信息通过 Model 放到 request 域中，以方便在页面中展示异常信息
        model.addAttribute("ex", exception);
        //跳转到错误页
        return "error";
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public String handleException2(Exception exception, Model model) {
        //将异常信息通过 Model 放到 request 域中，以方便在页面中展示异常信息
        model.addAttribute("ex", exception);
        //跳转到错误页
        return "error-2";
    }
}
