package cn.vdeer.controller;

import cn.vdeer.dao.UserDao;
import cn.vdeer.entity.User;
import cn.vdeer.exception.UserNotExistException;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
    @Resource
    private UserDao userDao;

    @RequestMapping("/login")
    public String login(String userName, Model model) {
        User user = userDao.getUserByUserName(userName);
        if (user == null) {
            throw new UserNotExistException();
        }
        return "success";
    }
}
