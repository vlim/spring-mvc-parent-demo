package cn.vdeer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

@Controller
public class TestExceptionController {
    /**
     * 测试 DefaultHandlerExceptionResolver（Spring MVC 默认的异常处理器）
     *
     * @return
     */
    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    public String testDefaultHandlerExceptionResolver() {
        return "success";
    }

}
