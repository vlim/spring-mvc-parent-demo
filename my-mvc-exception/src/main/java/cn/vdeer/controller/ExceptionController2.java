package cn.vdeer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExceptionController2 {

    //控制器方法
    @RequestMapping("/testExceptionHandler")
    public String testExceptionHandler() {
        //发生 ArithmeticException 异常
        System.out.println(1 / 0);
        return "success";
    }

    //使用 @ExceptionHandler 注解定义一个异常处理方法
    @ExceptionHandler(ArithmeticException.class)
    public String handleException(ArithmeticException exception, Model model) {
        //将异常信息通过 Model 放到 request 域中，以方便在页面中展示异常信息
        model.addAttribute("ex", exception);
        return "error";
    }
}
