package cn.vdeer.controller;

import cn.vdeer.formatter.MyDateFormatter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Controller
public class MyController {
    @GetMapping("/date")
    public String getDate(Model model, @RequestParam("date") @DateTimeFormat(pattern = "yyyy:MM:dd") Date date) {
        model.addAttribute("date", date);
        System.out.println(date);
        return "success";
    }
}
