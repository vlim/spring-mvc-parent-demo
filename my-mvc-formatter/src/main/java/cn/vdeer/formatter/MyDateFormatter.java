package cn.vdeer.formatter;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 格式化器
 * 将字符串表示的日期转换为 java.util.Date 对象，并将 java.util.Date 对象格式化为字符串
 */
public class MyDateFormatter implements Formatter<Date> {
    private String dataFormatterPattern;

    public MyDateFormatter(String dataFormatterPattern) {
        this.dataFormatterPattern = dataFormatterPattern;
    }

    /**
     * 将字符串表示的日期转换为 java.util.Date 对象
     *
     * @param text
     * @param locale
     * @return
     * @throws ParseException
     */
    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(dataFormatterPattern, locale);
        sdf.setLenient(false);
        System.out.println("Date text:" + text);
        return sdf.parse(text);
    }

    /**
     * 将 java.util.Date 对象格式化为字符串
     *
     * @param object
     * @param locale
     * @return
     */
    @Override
    public String print(Date object, Locale locale) {
//        SimpleDateFormat sdf = new SimpleDateFormat(dataFormatterPattern, locale);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年，MM月，dd日", locale);
        System.out.println("Date:" + object);
        return sdf.format(object);
    }
}
