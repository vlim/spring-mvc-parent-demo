package cn.vdeer.converter;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDateConverter implements Converter<String, Date> {
    private String datePatten = "yyyy-MM-DD";

    @Override
    public Date convert(String source) {
        System.out.println("前端页面传递过来的时间为：" + source);
        SimpleDateFormat sdf = new SimpleDateFormat(datePatten);
        try {
            return sdf.parse(source);
        } catch (ParseException e) {
            throw new IllegalArgumentException("无效的日期格式，请使用正确的日期格式" + datePatten);
        }
    }
}
