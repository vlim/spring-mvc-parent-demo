package cn.vdeer.controller;

import cn.vdeer.entity.User;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Controller
public class UserController {
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(User user, Model model) {
        System.out.println(user);
        model.addAttribute("user", user);
        return "success";
    }

    @RequestMapping(value = "/login1", method = RequestMethod.POST)
    public String login1(HttpServletRequest request) {
        User user = new User();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD");
        user.setUserName(request.getParameter("userName"));
        try {
            user.setBirth(sdf.parse(request.getParameter("birth")));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        user.setHeight(Double.valueOf(request.getParameter("height")));
        request.setAttribute("user", user);
        return "success";
    }
}
